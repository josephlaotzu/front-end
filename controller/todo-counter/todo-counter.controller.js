(function () {
  'use strict';

  angular.module('App')
    .directive('toDoCounter', toDoCounter);

  function toDoCounter() {
    return {
      bindToController: true,
      controller: ToDoCounterController,
      controllerAs: 'vm',
      restrict: 'EA',
      scope: {
        value: '=',
        counterType: '=?',
      },
      template: `<div style="text-align: center;">
        <h1><strong>{{vm.counterType === "completed" ? vm.completed : vm.incomplete}}</strong></h1>
          <h3>Todo {{vm.counterType === "completed" ? 'Completed': 'Not Complete'}}</h3>
        </div>`,
    };
  }

  function ToDoCounterController() {
    let vm = this;
    vm.completed = 0; vm.incomplete = 0;
    vm.$onInit = () => {
      _.forEach(vm.value, (value) => {
        if (value.todo.completed) {
          vm.completed++;
        } else {
          vm.incomplete++;
        }
      });
    }
  }

})();