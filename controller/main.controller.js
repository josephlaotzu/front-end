(function () {
  'use strict';

  angular.module('App', ['datatables', 'nvd3'])
    .controller('MainController', MainController);

  function MainController(ToDoApiService) {
    let vm = this;
    vm.load = false; // loader feature to be added
    ToDoApiService.getAllData().then((data) => {
      vm.data = data;
      vm.load = true;
    });
  }

})();