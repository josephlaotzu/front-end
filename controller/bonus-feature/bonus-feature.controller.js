(function () {
  'use strict';

  angular.module('App')
    .controller('BonusFeatureController', BonusFeatureController);

  function BonusFeatureController($scope, ToDoApiService) {
    var vm = this;
    vm.filter = 'completed';
    vm.data = $scope.$parent.$parent.vm.data;
    vm.onFilter = filtering;

    $scope.options = {
      chart: {
        type: 'pieChart',
        height: 500,
        x: function (d) { return d.x; },
        y: function (d) { return d.y; },
        showLabels: true,
        labelType: 'percent',
        transitionDuration: 1500,
        labelThreshold: 0.01,
        duration: 500,
        labelSunbeamLayout: true,
        legend: {
          margin: {
            top: 5,
            right: 35,
            bottom: 5,
            left: 0
          }
        }
      }
    };

    $scope.data = ToDoApiService.getGraphFormattedData(vm)[0].values;

    function filtering() {
      $scope.data = ToDoApiService.getGraphFormattedData(vm)[0].values;
    }
  }
})();