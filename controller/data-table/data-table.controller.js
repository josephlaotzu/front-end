(function () {
  'use strict';

  angular.module('App')
    .component('tableData', {
      controller: DataTableController,
      controllerAs: 'vm',
      bindToController: true,
      restrict: 'E',
      bindings: {
        value: '=',
      },
      template: (`<table datatable="ng" dt-options="vm.dtOptions" dt-column-defs="vm.dtColumnDefs" class="row-border hover">
                    <thead>
                      <tr>
                      <th>ID</th>
                      <th>User</th>
                      <th>Title</th>
                      <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in ::vm.value">
                        <td>{{ data.todo.id }} </td>
                        <td>{{ data.user.name }}</td>
                        <td>{{ data.todo.title }}</td>
                        <td>{{ data.todo.completed ? 'Completed ✓' : 'Not Completed ✗' }}</td>
                      </tr>
                    </tbody>
                </table>`), // template literal syntax
    });

  function DataTableController($scope, DTOptionsBuilder, DTColumnDefBuilder) {
    let vm = this;
    vm.$onInit = () => {
      vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDisplayLength(10);

        vm.dtColumnDefs = [
          DTColumnDefBuilder.newColumnDef(0).notVisible(),
          DTColumnDefBuilder.newColumnDef(1).withOption('width', '25%'),
          DTColumnDefBuilder.newColumnDef(2).withOption('width', '50%')
      ];
    };
  }
})();