(function () {
  'use strict';

  angular.module('App')
    .controller('BarGraphController', BarGraphController);

  function BarGraphController($scope, ToDoApiService) {
    let vm = this;
    vm.filter = 'all';
    vm.onFilter = filtering;
    vm.data = $scope.$parent.$parent.vm.data;

    $scope.options = {
      chart: {
        type: 'multiBarChart',
        height: 450,
        margin: {
          top: 30,
          right: 20,
          bottom: 65,
          left: 50
        },
        clipEdge: true,
        duration: 1500,
        stacked: false,
        showControls: false,
        reduceXTicks: false,
        xAxis: {
          axisLabel: 'Users',
          rotateLabels: -15,
          showMaxMin: false,
          tickFormat: function (d) {
            return (d);
          }
        },
        yAxis: {
          axisLabel: 'No. of todo\'s',
          axisLabelDistance: -20,
          ticks: 10,
          tickFormat: function (d) {
            return (d);
          }
        }
      }
    };

    $scope.data = ToDoApiService.getGraphFormattedData(vm);

    function filtering() {
      $scope.data = ToDoApiService.getGraphFormattedData(vm);
    }

  }
})();