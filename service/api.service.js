(function () {
  'use strict';

  angular.module('App')
    .factory('ToDoApiService', ['$http', '$q', ToDoApiService]);

  function ToDoApiService($http, $q) {
    const url = param => `https://jsonplaceholder.typicode.com/${param}`;

    return {
      getToDoData: getToDoData,
      getUserData: getUserData,
      getAllData: getAllData,
      getGraphFormattedData: getGraphFormattedData
    };

    function getAllData() {
      let call = [getToDoData(), getUserData()];
      return $q.all(call).then((response) => {
        return joinUserAndTodoData(response);
      }).catch((error) => {
        handleError(error);
      });
    }

    function getToDoData() {
      return $http.get(url('todos')).then((response) => {
        return response.data;
      }).catch((error) => {
        handleError(error);
      });
    }

    function getUserData() {
      return $http.get(url('users')).then((response) => {
        return response.data
      }).catch((error) => {
        handleError(error);
      });
    }

    function joinUserAndTodoData(data) {
      let joined = [];
      _.forEach(data[0], (todo) => {
        let join = {
          user: _.filter(data[1], ['id', todo.userId])[0],
          todo: todo
        }
        joined.push(join)
      })
      return joined;
    }

    function getGraphFormattedData(vm) {

      // set defaults to bar graph data
      let completed = {
        key: "Completed", color: "#18AF5C", values: []
      };
      let incomplete = {
        key: "Not Completed", color: "#B11820", values: []
      };
      let total = {
        key: "Total", color: "#F0AD4E", values: []
      };

      let todoPerUser = [];
      let count = _.countBy(vm.data, 'user.id'); // get user count
      _.forEach(count, (d, i) => { // get todos per user
        let v = _.filter(vm.data, function (o) {
          return o.todo.userId == i;
        })
        todoPerUser.push(v);
      });

      _.forEach(todoPerUser, function (val) { // filter user todo to completed/not completed
        let comp = _.filter(val, ['todo.completed', true]).length;
        let inc = _.filter(val, ['todo.completed', false]).length;
        let user = _.filter(val, function (d, i) { return d.user.id === i + 1; });

        completed.values.push({ y: comp, x: user[0].user.name });
        incomplete.values.push({ y: inc, x: user[0].user.name });
        total.values.push({ y: comp + inc, x: user[0].user.name })
      });

      switch (vm.filter.toLowerCase()) {
        case 'completed':
          return [completed];
        case 'incomplete':
          return [incomplete];
        default:
          return _.concat(completed, incomplete, total);
      }

    }

    function handleError(error) {
      // statusText should return error message but this sample api doesnt
      if (error.status === 404) {
        error.statusText = 'Not Found';
      }
      alert(Error(error.statusText));
    }
  }
})();